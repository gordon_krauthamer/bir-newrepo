## BIR Repo

This is BIR's repo.

## Requirements
1. Have Node.js installed
2. Have Bower installed
3. Have NPM installed
4. Have gulp installed Globally
5. Run gulp-live-server

## Project Setup  

1. Fork the repo

~~~
git fork
~~~

2. Clone the repo

~~~
git clone
~~~

3. Install Node dependencies

~~~
$ npm install
~~~

4. Install Bower dependencies

~~~
$ bower install
~~~


## Usage

1. First terminal run gulp serve

~~~
$ gulp serve
~~~

2. Second terminal run gulp

~~~
$ gulp
~~~

That's it!
